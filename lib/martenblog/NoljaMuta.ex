defmodule Martenblog.NoljaMuta do
  require Logger
  use GenServer

  def start_link(default) when is_map(default) do
    GenServer.start_link(__MODULE__, default)
  end

  @impl true
  def init(%{interval: interval}=state) do
    :timer.send_interval(interval * 1000, :run)
    {:ok, state}
  end

  @impl true
  def handle_info(:run, %{f: f}=state) do
    # Logger.info "NoljaMuta.ex > handle_info"
    f.()
    {:noreply, state}
  end
end
