defmodule Martenblog.Nostr do
  require Logger
  @pubkey "a3fa67c734c107cf40f682d350d038c112b0599ad97b04bd24de57e8ff769276"
  @url "wss://nostr.thurk.org"


  def to_bitstring(s) do
    cabeza = s |> String.slice(0, 2)
    rabo = s |> String.slice(2, 256)
    {n, _} = cabeza |> Integer.parse(16)
    if String.length(rabo) < 2 do
      <<n>>
    else
      <<n>> <> to_bitstring(rabo)
    end
  end

  def serial_structure(text, created_at, tags \\ []) do
    [
      0,
      @pubkey,
      created_at,
      1,
      tags,
      text
    ]
  end

  def json_for_id(text, created_at, tags \\ []) do
    serial_structure(text, created_at, tags) |> Poison.encode!
  end

  def id_for_note(text, created_at, tags \\ []) do
    json_for_id(text, created_at, tags) |> 
      (fn json -> :crypto.hash(:sha256, json) end).() |> Base.encode16(case: :lower)
  end

  def make_note(text, created_at) do
    %{
      id: id_for_note(text, created_at),
      pubkey: @pubkey,
      created_at: created_at,
      kind: 1,
      tags: [],
      content: text
    }
  end
  def make_note(text) do
    make_note(text, DateTime.utc_now |> DateTime.to_unix)
  end

  def note_event(text, created_at) do
    [
      "EVENT",
      make_note(text, created_at)
    ] |> (fn event ->
      encoded_event = event |> Poison.encode!
      Logger.info "Encoded event: #{encoded_event}"
      System.shell("echo #{encoded_event} | websocat #{@url}")
    end).()
  end
  def note_event(text) do
    note_event(text, DateTime.utc_now |> DateTime.to_unix)
  end

  ### Nothing above here is used any longer

  def note(%{content: content, created_at: created_at}, privkey) do
    text = content |> String.replace(~r{"}, "'")
    command = "nostril --envelope --created-at #{created_at} --sec #{privkey} --content \"#{text}\" | websocat #{@url}"
    case System.shell(command) do
      {res, 0} -> res
      _ -> {:error, "eh?"}
    end
  end
  def note(%{content: content}, privkey), do: note(%{content: content, created_at: DateTime.utc_now |> DateTime.to_unix}, privkey)

  def smoothen_event([
    "EVENT", _,
    %{
      "content" => content,
      "created_at" => created_at
    }
  ]) do
    %{
      content: content,
      created_at: created_at |> DateTime.from_unix! |> Calendar.strftime("%Y-%m-%d %H:%M"),
      timestamp: created_at
    }
  end
  def smoothen_event(_), do: nil

  def get_events do
    json = [ "REQ", "thurk", %{ authors: [ @pubkey ] } ] |> Poison.encode!
    Logger.info "Nostr.ex > get_events > json: #{json}"
    case System.shell("echo '#{json}' | websocat #{@url}") do
      {res, 0} -> 
        res |> 
          String.split(~r{\n}) |>
          Enum.reject(fn s -> s |> String.match?(~r{^\s*$}) end) |>
          Enum.map(fn s -> s |> Poison.decode! end)  |>
          Enum.map(&smoothen_event/1) |>
          Enum.reject(fn event -> is_nil(event) end) |>
          Enum.reject(fn event -> 
            event |> 
              Map.get(:content, "") |> 
              String.trim |> 
              String.length |> 
              Kernel.==(0) 
          end)
       _ -> {:error, "eh?"}
    end
  end
end
