defmodule Martenblog.Lakife do
  @prefix "/home/polaris/various-leprosies/draining_the_pond/lakife"
  @vocab_file Path.join(@prefix, "vocabulary.md")
  @phrases_file Path.join(@prefix, "phrases.md")
  @idioms_file Path.join(@prefix, "idioms.md")
  @abbreviations_file Path.join(@prefix, "xian-abbreviated.md")

  def vocab do
    @vocab_file |> File.read |> case do
      {:ok, file_as_string} ->
        file_as_string |>
          String.split(~r{\n}) |>
          Enum.map(fn line ->
            trimmed = line |> String.trim
            case Regex.run(
              ~r{^(?<lakife>.+)\s-\s(?<english>.+)$}, 
              trimmed,
              capture: ~w(lakife english)a
            ) do
              [lakife, english] -> %{lakife: lakife, english: english}
              nil -> nil
            end
          end) |> Enum.reject(fn e -> is_nil(e) end)
      {:error, _} -> []
    end
  end

  def abbreviated do
    @abbreviations_file |> File.read |> case do
      {:ok, file_as_string } -> 
        file_as_string |>
          String.split(~r{\n}) |>
          Enum.reduce({[], %{}},
            fn line, {res, incomplete} ->
              cond do
                line |> String.match?(~r{^---}) ->
                  {[ incomplete | res ], %{}}
                line |> String.match?(~r{^-\s}) ->
                  trimmed = line |> String.slice(1..-1) |> String.trim
                  key = case incomplete do
                    %{lakife: _} -> :english
                    _ -> :lakife
                  end
                  new_incomplete = incomplete |> Map.put(key, trimmed)
                  {res, new_incomplete}
                true -> {res, incomplete}
              end
            end)
      {:error, _} -> {[], %{}}
    end |> case do
      {res, %{}} -> res
      {res, mut} -> [ mut | res ]
    end
  end

  def phrases do
    @phrases_file |> File.read |> case do
      {:ok, file_as_string} ->
        file_as_string |>
          String.split(~r{\n}) |>
          Enum.reduce({[], %{}}, fn line, {res, incomplete} ->
            cond do
              line |> String.match?(~r{^---}) ->
                {[ incomplete | res ], %{}}
              line |> String.match?(~r{^-\s}) ->
                trimmed = line |> String.slice(1..-1) |> String.trim
                new_incomplete = case incomplete do
                  %{lakife: _} ->
                    incomplete |> Map.put(:english, trimmed)
                  _ -> incomplete |> Map.put(:lakife, trimmed)
                end
                {res, new_incomplete}
              true -> {res, incomplete}
            end
          end)
      {:error, _} -> {[], %{}}
    end |> (fn {res, incomplete} ->
      if Enum.count(Map.keys(incomplete)) > 0 do
       [ incomplete | res ]
      else
        res
      end
    end).()
  end

  def idioms do
    @idioms_file |> File.read |> case do
      {:ok, file_as_string} ->
        file_as_string |>
          String.split(~r{\n}) |>
          Enum.reduce({[], %{}}, fn line, {res, incomplete} ->
            cond do
              line |> String.match?(~r{^---}) ->
                {[ incomplete | res ], %{}}
              line |> String.match?(~r{^-\s}) ->
                trimmed = line |> String.slice(1..-1) |> String.trim
                new_incomplete = case incomplete do
                  %{english: _, lakife: _} ->
                    incomplete |> Map.put(:explanation, trimmed)
                  %{english: _} ->
                    incomplete |> Map.put(:lakife, trimmed)
                  _ -> incomplete |> Map.put(:english, trimmed)
                end
                {res, new_incomplete}
              true -> {res, incomplete}
            end
          end)
      {:error, _} -> {[], %{}}
    end |> (fn {res, incomplete} ->
      if Enum.count(Map.keys(incomplete)) > 0 do
       [ incomplete | res ]
      else
        res
      end
    end).()
  end
end
