defmodule Martenblog.Gemini do
  require Logger
  alias Martenblog.{Utils, Tema, Anotacion}
  @releases_dir "/home/polaris/Elements/flavigula/release/"
  @poems_dir "/home/polaris/arch-my-hive/poems/"
  @gemini_public "/usr/share/molly/"
  @gemini_poems "/usr/share/molly/poems"
  # @gemini_cgi "/usr/share/molly/cgi-bin/"

  def poem_to_gemini(poem_path) do
    case File.read(poem_path) do
      {:ok, lines} -> 
        lines |> String.split(~r/\n/) |>
          Enum.reduce(%{after_header: false, text: ""},
            fn line, acc ->
              cond do
                acc.after_header -> %{
                  after_header: true,
                  date: Map.get(acc, :date, nil),
                  title: Map.get(acc, :title, "Untitled"),
                  text: "#{acc.text}#{String.capitalize(line)}\n"
                }
                Regex.match?(~r/^-/, line) ->
                  %{
                    after_header: true, 
                    date: Map.get(acc, :date, nil),
                    title: Map.get(acc, :title, "Untitled"),
                    text: "#{acc.text}\n```\n"
                  }
                Regex.match?(~r/^\d\d\d\d-/, line) ->
                  %{
                    after_header: false,
                    date: String.trim(line),
                    title: Map.get(acc, :title, "Untitled"),
                    text: "#{acc.text}## #{line}\n"
                  }
                true ->
                  %{
                    after_header: false,
                    date: Map.get(acc, :date, nil),
                    title: String.trim(line),
                    text: "#{acc.text}# #{line}\n"
                  }
              end
            end) |>
            (fn ballyhoo -> 
              %{
                date: Map.get(ballyhoo, :date, "1970-01-01"),
                title: Map.get(ballyhoo, :title, "Unknown"),
                text: """
#{ballyhoo.text}
```

=> index.gmi tzifur
=> gemini://thurk.org jeniz
"""
              }
            end).()
      _ -> nil
    end
  end

  def dash_title(s) do
    String.split(s, ~r/\s+/) |> Enum.join("-")
  end

  def write_poems_index(poems) do
    body = poems |> Enum.reduce("", fn poem, acc ->
      "#{acc}=> #{dash_title(poem.title)}-#{poem.date}.gmi #{poem.title} (#{poem.date})\n"
    end)
    text = """
    # Poems

    #{body}

    => ../index.gmi tzifur
    """
    File.write(Path.join(@gemini_poems, "index.gmi"), text)
    poems
  end

  def poems_to_gemini do
    case File.ls(@poems_dir) do
      {:ok, files} ->
        files |>
          Enum.reduce([], fn filename, acc ->
            if Regex.match?(~r/.txt$/, filename) do
              Logger.info "Processing #{filename}"
              [Path.join(@poems_dir, filename) |> poem_to_gemini | acc]
            else
              acc
            end
          end) |> Enum.sort(&(&1.date >= &2.date)) |>
          write_poems_index |>
          Enum.each(fn ballyhoo ->
            title = dash_title(ballyhoo.title)
            File.write(Path.join(@gemini_poems, "#{title}-#{ballyhoo.date}.gmi"), ballyhoo.text)
          end)
      _ -> nil
    end
  end

  def blog_entry_to_gemini(entry) do
    entry |>
    (fn(entry) ->
      if !is_nil(entry) do
        filename = "/tmp/#{Utils.randomFilename}"
        File.write!(filename, entry.entry)
        thurk = Port.open(
          {:spawn, "md2gemini -l at-end #{filename}"},
          [:binary]
        )
        topics = Tema.topics_by_names(entry.topic) |> 
          Enum.map(fn t -> 
            if !is_nil(t) do
              Map.get(t, :topic, "nulu") 
            else
              "nulu"
            end
          end) |> 
          Enum.join(", ")
        receive do
          {^thurk, {:data, result_temp}} ->
            result = 
              String.replace(result_temp, ~r/(\w)\^\w/, "\\1") |>
              String.replace(~r</images/blog>, "https://flavigula.net/images/blog")
            """
            # #{entry.subject}
            ## Topics: #{topics}
            ## #{Utils.format_mb_date(entry.date)}

            #{result}

            => gemini://thurk.org/blog/index.gmi tzifur (Martenblog home)
            => gemini://thurk.org/index.gmi jenju (Thurk.Org home)

            @flavigula@sonomu.club
            CC BY-NC-SA 4.0
            """
        end |> (fn text -> File.write!(Path.join(@gemini_public, "blog/#{floor(entry.id)}.gmi"), text) end).()
      end
    end).()
  end

  def make_index(entries) do
    entries |> Enum.map(fn e ->
      "=> #{e.id}.gmi #{Utils.format_mb_date(e.date)} - #{e.subject}\n"
    end) |> (fn subject_list ->
      affirmation = """
      # Here lies Martes Flavigula, eternally beneath the splintered earth
      ## Otherwise known as the Martenblog

      => gemini://thurk.org/cgi-bin/martenblog-search.py search
      => gemini://thurk.org/index.gmi tzifur

      #{subject_list}

      => gemini://thurk.org/blog/index.gmi tzifur (Martenblog home)
      => gemini://thurk.org/index.gmi jenju (Thurk.Org home)

      @flavigula@sonomu.club

      ### Along with goulish goats and the rippling fen -
      ### these writings 1999-2023 by Bob Murry Shelton are licensed under CC BY-NC-SA 4.0
      """
      File.write!(Path.join(@gemini_public, "blog/index.gmi"), affirmation)
    end).()
  end

  def make_gemini_feed do
    Anotacion.some(53) |> Enum.map(fn e ->
      "=> #{e.id}.gmi #{Utils.format_mb_date(e.date)} #{e.subject}\n"
    end) |> (fn subject_list ->
      affirmation = """
      # Martenblog
      ## The musings of a mustelid
      ## Or, here lies Martes Flavigula, eternally beneath the splintered earth

      #{subject_list}

      => gemini://thurk.org/blog/index.gmi tzifur (Martenblog home)
      => gemini://thurk.org/index.gmi jenju (Thurk.Org home)

      @flavigula@sonomu.club

      ### Along with goulish goats and the rippling fen -
      ### these writings 1999-2023 by Bob Murry Shelton are licensed under CC BY-NC-SA 4.0
      """
      File.write!(Path.join(@gemini_public, "blog/feed.gmi"), affirmation)
      File.write!(Path.join(@gemini_public, "blog/atom.xml"), Martenblog.Atom.atom)
    end).()
  end

  def slap_um_in do
    entries = Anotacion.all
    make_index(entries)
    entries |> Enum.each(fn e -> blog_entry_to_gemini(e) end)
  end
  def slap_um_in(limit) do
    entries = Anotacion.some(limit)
    make_index(entries)
    entries |> Enum.each(fn e -> blog_entry_to_gemini(e) end)
  end

  defp join_to_release_dir(thurk) do
    Path.join(@releases_dir, thurk)
  end

  defp unpath(p) do
    Path.basename(p) |> (fn filename -> Regex.scan(~r/^(.+)\..+$/, filename) end).() |> (fn m -> case m do
      [] -> Path.basename(p)
      [[_, name]] -> name
      _ -> Path.basename(p)
    end
    end).()
  end

  defp normalise_title(t) do
    t |> String.trim |> (fn s ->
      case Regex.scan(~r/^(.+)_session.+$/, s) do
        [[_, name]] -> name
        [] -> s
      end
    end).() |> (fn s ->
      case Regex.scan(~r/^\d+[-\s]+(.+)$/, s) do
        [[_, name]] -> name
        [] -> s
      end
    end).() |> (fn s ->
      case Regex.scan(~r/^(.+)-range.*$/, s) do
        [[_, name]] -> name
        [] -> s
      end
    end).() |>
    String.split(~r/[-\s]/) |> Enum.map(fn s -> String.capitalize(s, :default) end) |> Enum.join(" ")
  end

  def release_order_or_ls do
    if File.exists?(join_to_release_dir(".order")) do
      File.read!(join_to_release_dir(".order")) |> String.split(~r/\n/) |> 
      Enum.map(fn t -> String.trim(t) end) |>
      Enum.filter(fn t -> String.length(t) > 0 end) |>
      (fn dir_names -> {:ok, dir_names} end).()
    else
      case File.ls(@releases_dir) do
        {:ok, album_dirs} ->
          {:ok, Enum.reject(album_dirs, fn t -> Regex.match?(~r/^\./, t) end)}
        _ -> {:error, "Nothing found"}
      end
    end
  end

  def releases_main do
    case release_order_or_ls() do
      {:ok, album_dirs} ->
        Enum.map(album_dirs, fn dir -> 
          at = if File.exists?(join_to_release_dir("#{dir}/title")) do
            File.read!(join_to_release_dir("#{dir}/title"))
          else
            dir
          end |> normalise_title
          album_title = "## #{at}\n"
          description = case File.read(join_to_release_dir("#{dir}/description")) do
            {:ok, description} -> "#{description}\n"
            _ -> ""
          end
          date = if File.exists?(join_to_release_dir("#{dir}/date")) do
            d = File.read!(join_to_release_dir("#{dir}/date"))
            "#{d}\n"
          else
            ""
          end
          link = "#{dir}.gmi"
          make_single_release(dir, link, at, description, date)
          "#{album_title}\n#{date}#{description}=> #{link} #{at}\n"
        end)
      _ -> ["# Flavigula Releases\n\n### This space intentionally left to the void."]
    end |> Enum.join("-------------------------------------------------------------------------------\n") |>
    (fn text ->
      argument = """
      # Flavigula Releases
      
      #{text}
      
      => gemini://thurk.org/flavigula/index.gmi tzifur (Flavigula home)
      => gemini://thurk.org/index.gmi jenju (Thurk.Org home)

      @flavigula@sonomu.club
      CC BY-NC-SA 4.0
      """
      File.write!(Path.join(@gemini_public, "flavigula/releases.gmi"), argument)
    end).()
  end

  def releases do
    releases_main()
  end

  def make_single_release(dir, link, album_title, description, date) do
    case File.ls(join_to_release_dir(dir)) do
      {:ok, files} ->
        Enum.reduce(files, %{zip: "", tracks: []}, fn file, acc ->
          if Regex.match?(~r/zip$/, file) do
            Map.merge(acc, %{zip: "release/#{dir}/#{file}"})
          else
            case File.ls(join_to_release_dir("#{dir}/#{file}")) do
              {:ok, tracks} ->
                Enum.reduce(tracks, acc, fn track, acc ->
                  if Regex.match?(~r/(flac|mp3|ogg|wav)$/, track) do
                    Map.merge(acc, %{tracks: ["release/#{dir}/#{file}/#{track}" | Map.get(acc, :tracks)]})
                  else
                    acc
                  end
                end)
              _ -> acc
            end
          end
        end)
      _ -> %{error: "### This album is intentionally part of the void."}
    end |> (fn result ->
      album_body = case result do
        %{error: error} -> error
        %{zip: zip, tracks: tracks} ->
          full_album_text = "=> #{zip} Full Album"
          tracks_text = Enum.reverse(tracks) |>
          Enum.map(fn t -> "=> #{t} #{unpath(t) |> normalise_title}" end) |>
          Enum.join("\n")
          """
          #{tracks_text}
          """
        _ -> "### You have died the flame death, vole"
      end
      argument = """
      # #{album_title}
      ## #{date}

      #{description}

      #{album_body}

      => gemini://thurk.org/flavigula/releases.gmi tzifur (Flavigula releases)
      => gemini://thurk.org/index.gmi jenju (Thurk.Org home)

      @flavigula@sonomu.club
      CC BY-NC-SA 4.0
      """
      File.write!(Path.join(@gemini_public, "flavigula/#{link}"), argument)
    end).()
  end

  def releases_old do
    case File.ls("/home/polaris/Elements/flavigula/release") do
      {:ok, album_dirs} ->
        Enum.map(album_dirs, fn dir -> 
          album_title = if File.exists?(join_to_release_dir("#{dir}/title")) do
            File.read!(join_to_release_dir("#{dir}/title"))
          else
            dir
          end |> normalise_title
          description = case File.read(join_to_release_dir("#{dir}/description")) do
            {:ok, description} -> "#{description}\n"
            _ -> ""
          end
          album_header = "\n## #{album_title}\n#{description}"
          case File.ls(join_to_release_dir(dir)) do
            {:ok, files} ->
              Enum.reduce(files, %{zip: "", tracks: []}, fn file, acc ->
                if Regex.match?(~r/zip$/, file) do
                  Map.merge(acc, %{zip: "release/#{dir}/#{file}"})
                else
                  case File.ls(join_to_release_dir("#{dir}/#{file}")) do
                    {:ok, tracks} ->
                      Enum.reduce(tracks, acc, fn track, acc ->
                        if Regex.match?(~r/(flac|mp3|ogg|wav)$/, track) do
                          Map.merge(acc, %{tracks: ["release/#{dir}/#{file}/#{track}" | Map.get(acc, :tracks)]})
                        else
                          acc
                        end
                      end)
                    _ -> acc
                  end
                end
              end)
            _ -> %{ error: "### This album is intentionally part of the void." }
          end |> (fn result ->
            album_body = case result do
              %{error: error} -> error
              %{zip: zip, tracks: tracks} ->
                full_album_text = "=> #{zip} Full Album"
                tracks_text = Enum.reverse(tracks) |> 
                  Enum.map(fn t -> "=> #{t} #{unpath(t) |> 
                  normalise_title}" end) |> Enum.join("\n")
                """
                #{full_album_text}
                #{tracks_text}
                """
              _ -> "### Something has gone terribly wrong, vole"
            end
            "#{album_header}#{album_body}"
          end).()
        end) |> Enum.join("\n")
      _ -> "# Flavigula Releases\n\n### This space intentionally left to the void."
    end |> (fn text -> 
      argument = """
      # Flavigula Releases
      
      #{text}
      
      => gemini://thurk.org/flavigula/index.gmi tzifur (Flavigula home)
      => gemini://thurk.org/index.gmi jenju (Thurk.Org home)

      @flavigula@sonomu.club
      CC BY-NC-SA 4.0
      """
      File.write!("/home/polaris/src/blizanci/public_gemini/flavigula/releases.gmi", argument) 
    end).()
  end
end
