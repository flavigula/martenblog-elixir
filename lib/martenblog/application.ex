defmodule Martenblog.Application do
  use Application
  alias Martenblog.NoljaMuta
  alias Martenblog.Twtxt

  def start(_type, _args) do
    # import Supervisor.Spec

    IO.inspect(:erlang.node)
    IO.inspect(:erlang.get_cookie)
    :ets.new(:searches, [:bag, :public, :named_table])

    children = [
      {Plug.Cowboy, scheme: :http, plug: Martenblog.Router, options: [port: 8777]},
      {Plug.Cowboy, scheme: :http, plug: Martenblog.Websocket, options: [port: 8778, dispatch: dispatch()]},
      {NoljaMuta, %{
        interval: 1860,
        f: fn -> Twtxt.from_nostr(nil) end
      }}
    ]

    Application.ensure_all_started(:hackney)
    opts = [strategy: :one_for_one, name: Martenblog.Supervisor]
    Supervisor.start_link(children, opts)
    # Martenblog.Router.start_link
  end

  defp dispatch do
    [
      {:_,
        [
          {"/ws/[...]", Martenblog.SocketHandler, []},
          {:_, Plug.Cowboy.Handler, {Martenblog.Websocket, []}}
        ]
      }
    ]
  end
end
