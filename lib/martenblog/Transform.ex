defmodule Martenblog.Transform do
  defmacro gemini do
    quote do 
      [
        %{
          dir: "flavigula",
          template: "music",
          geminis: [
            %{
              file: "index",
              options: %{head_lop: 1, footnote_links: true}
            }, 
            %{
              file: "releases",
              options: %{footnote_links: true}
            }
          ]
        },
        %{
          dir: "recipes",
          template: "recipes",
          geminis: [
            %{
              file: "gofres",
              options: %{footnote_links: true}
            },
            %{
              file: "mango-salsa",
              options: %{footnote_links: true}
            },
            %{
              file: "yucatan-chicken",
              options: %{footnote_links: true}
            },
            %{
              file: "index",
              options: %{head_lop: 1, footnote_links: true}
            }
          ]
        },
        %{
          dir: "drone",
          template: "drone",
          geminis: [
            %{
              file: "index",
              options: %{head_lop: 1, footnote_links: true}
            }
          ]
        }
      ]
    end
  end
end
